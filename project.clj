(defproject link "0.1.0-SNASHOT"
  :description "A straightforward (not-so-clojure) clojure wrapper for java nio framework"
  :dependencies [[org.clojure/clojure "1.3.0"]
                 [io.netty/netty "3.3.1.Final"]])

